Extendible Sokoban
==================
ext-soko
--------
L’obiettivo è ricreare il videogioco Sokoban e renderlo estendibile dagli stessi utenti, offrendo la possibilità di creare livelli personalizzati, salvarli, importarli e ordinarli in sequenze di livelli personalizzate.

### Funzionalità minime:
* Implementazione del gioco Sokoban classico con alcuni livelli di base a difficoltà crescente
* Interfaccia utente che consenta di creare un proprio livello e salvarlo
* Interfaccia utente che consenta di caricare uno o più livelli esistenti, ordinarli e giocarci
* Menu iniziale che consenta le funzionalità previste

### Funzionalità opzionali:
* Aggiunta di grafiche accattivanti e di suoni
* Salvataggio e importazione di sequenze di livelli
* Sistema di punteggi basati sul tempo di risoluzione di un livello con eventuale classifica dei punteggi migliori (e possibilità di reset)
* Possibilità di salvare la partita per riprenderla in un secondo momento

### Challenge principali
* Progettazione e implementazione del sistema di creazione e gestione dei livelli
* Uso del pattern MVC
* Osservanza delle best practices di progettazione ad oggetti e di programmazione Java

#### Credits
* Arrow up icon made by [Dave Gandy](https://www.flaticon.com/authors/dave-gandy), downloaded from [Flaticon](https://www.flaticon.com/).
* Arrow down icon made by [Dave Gandy](https://www.flaticon.com/authors/dave-gandy), downloaded from [Flaticon](https://www.flaticon.com/).
* Back icon made by [Google](https://www.flaticon.com/authors/google), downloaded from [Flaticon](https://www.flaticon.com/).
* Plus icon made by [Smashicons](https://www.flaticon.com/authors/smashicons), downloaded from [Flaticon](https://www.flaticon.com/).
* Minus icon made by [Smashicons](https://www.flaticon.com/authors/smashicons), downloaded from [Flaticon](https://www.flaticon.com/).
* Cross icon made by [Freepik](https://www.flaticon.com/authors/freepik), downloaded from [Flaticon](https://www.flaticon.com/).
* Ok icon made by [Dave Gandy](https://www.flaticon.com/authors/dave-gandy), downloaded from [Flaticon](https://www.flaticon.com/).
* Craft icon made by [Those icons](https://www.flaticon.com/authors/those-icons), downloaded from [Flaticon](https://www.flaticon.com/).
* Upload icon made by [Smashicons](https://www.flaticon.com/authors/smashicons), downloaded from [Flaticon](https://www.flaticon.com/).
* Download icon made by [Smashicons](https://www.flaticon.com/authors/smashicons), downloaded from [Flaticon](https://www.flaticon.com/).
* User icon made by [Freepik](https://www.flaticon.com/authors/freepik), downloaded from [Flaticon](https://www.flaticon.com/).
* Target icon made by [Dave Gandy](https://www.flaticon.com/authors/dave-gandy), downloaded from [Flaticon](https://www.flaticon.com/).
* Box icon made by [Smashicons](https://www.flaticon.com/authors/smashicons), downloaded from [Flaticon](https://www.flaticon.com/).
* Wall icon made by [Skyclick](https://www.flaticon.com/authors/skyclick), downloaded from [Flaticon](https://www.flaticon.com/).


